//
//  FilterCollection.swift
//  VideoFilter
//
//  Created by Shubham Kaliyar on 13/12/20.
//  Copyright © 2020 2Build. All rights reserved.
//

import Foundation
import UIKit

protocol FilterDidTabDelegate {
    func didUpdateFilter(_ filter:SKPlayer.SKFilter)
}

class FilterCollection: UICollectionView {
    
    var skPlayer = SKPlayer()
    var filterDelegate:FilterDidTabDelegate?
    
    // MARK:- life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setDelegate()
    }
    func setDelegate() {
        self.register(UINib(nibName: "FilterCell", bundle: nil), forCellWithReuseIdentifier: "FilterCell")
        self.delegate = self
        self.dataSource = self
    }
    
    
}
// MARK:- cextension of main class for CollectionView
extension FilterCollection : UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SKPlayer.SKFilter.allCases.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = SKPlayer.SKFilter.allCases[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as! FilterCell
        cell.lblNAme.text = filter.rawValue
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let filter = SKPlayer.SKFilter.allCases[indexPath.row]
        filterDelegate?.didUpdateFilter(filter)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 70)
    }
}
