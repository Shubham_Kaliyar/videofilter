//
//  SKPlayer.swift
//  VideoFilter
//
//  Created by Shubham Kaliyar on 13/12/20.
//  Copyright © 2020 2Build. All rights reserved.

/*
 //MARK:- Add This in Plist For Ns Security
 <key>NSAppTransportSecurity</key>
 <dict>
 <key>NSAllowsArbitraryLoads</key>
 <true/>
 <key>NSExceptionDomains</key>
 <dict>
 <key>https://www.youtube.com/watch?v=4rdby1KcCek</key>
 <dict>
 <key>NSExceptionAllowsInsecureHTTPLoads</key>
 <true/>
 <key>NSExceptionMinimumTLSVersion</key>
 <string>TLSv1.1</string>
 <key>NSIncludesSubdomains</key>
 <true/>
 </dict>
 </dict>
 </dict>
 */

import Foundation
import UIKit
import AVKit
import AVFoundation
import AudioToolbox


class SKPlayer : AVPlayer {
    //MARK:- Objects
    var player = AVPlayer()
    var playerAssets:AVAsset!
    var playerItem:AVPlayerItem!
    var playerLayer  = AVPlayerLayer()
    static let shared = SKPlayer()
    private var isEmptyBuffer: NSKeyValueObservation?
    private var isFullBuffer: NSKeyValueObservation?
    private var isLikelyBuffer: NSKeyValueObservation?
    fileprivate let seekDuration: Float64 = 5
    let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
    
    
    //MARK:- Function Of AV-Player For Play Video
    func playVideo(videoURL : String , view: UIView , repeatedVideo : Bool , isMuted:Bool = false) {
        let url = URL(string: videoURL)
        self.playerAssets = AVAsset(url: url!)
        self.playerItem = AVPlayerItem(asset: self.playerAssets)
        self.player = AVPlayer(playerItem: self.playerItem)
        //  self.player = AVPlayer(url: url!)
        self.playerLayer = AVPlayerLayer(player: player)
        self.playerLayer.videoGravity = .resizeAspectFill
        self.playerLayer.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        view.layer.addSublayer(playerLayer)
        view.contentMode = .scaleAspectFill
        view.layoutIfNeeded()
        self.showLoader(view: view)
        self.player.play()
        self.muteVideo(mute: isMuted)
        self.observeBuffering(for: self.player.currentItem!)
        
    }
    
    
    func appleFilter(filterName:SKFilter,inputCenter:CIVector? = nil,inputScale:NSNumber? = nil,inputIntencity:NSNumber? = nil){
        let filter = CIFilter(name: filterName.rawValue)
        self.playerItem.videoComposition = AVVideoComposition(asset: self.playerAssets, applyingCIFiltersWithHandler: { (resuest) in
            let source = resuest.sourceImage.clampedToExtent()
            filter?.setValue(source, forKey: kCIInputImageKey)
            if inputCenter != nil {
                filter?.setValue(inputCenter, forKey: kCIInputCenterKey)
            }
            if inputScale != nil {
                filter?.setValue(inputScale, forKey: kCIInputScaleKey)
            }
            if inputIntencity != nil {
                filter?.setValue(inputIntencity, forKey: kCIInputIntensityKey)
            }
            guard let output = filter?.outputImage?.cropped(to: resuest.sourceImage.extent) else {
                resuest.finish(with: CIImage(), context: nil)
                print("output not fatch")
                return
            }
            resuest.finish(with: output, context: nil)
        })
    }
    
    //MARK:- Function Of AV-Player For Play Video
    func playVideoOnScreen(videoURL : String , view: UIView, repeatedVideo : Bool , isMuted:Bool = true) {
        let url = URL(string: videoURL)
        self.player = AVPlayer(url: url!)
        self.playerLayer = AVPlayerLayer(player: self.player)
        self.playerLayer.videoGravity = .resizeAspect
        self.playerLayer.frame = UIScreen.main.bounds
        view.layer.addSublayer(self.playerLayer)
        self.player.isMuted = isMuted
        self.showLoader(view: view)
        self.player.play()
        self.observeBuffering(for: self.player.currentItem!)
        if repeatedVideo {
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: .main) { [weak self] _ in
                self?.player.seek(to: CMTime.zero)
                self?.player.play()
            }
        }
    }
    
    //Func for Avplayer Controller For Play Video
    func playVideoAvplayerController(videoUrl:String){
        guard let url = URL(string: videoUrl) else {return}
        print("Video url is -> ", url)
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        let shared = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        shared?.window?.rootViewController?.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    //MARK:- Function For MuteVideo
    func muteVideo(mute : Bool){
        player.isMuted = mute
    }
    //MARK:- Function For Pause Video
    func pauseVideo(pause:Bool) {
        pause ? self.player.pause() : self.player.play()
    }
    //MARK:- Function for restart Video
    func stopVideo() {
        self.muteVideo(mute:true)
        self.player.seek(to: CMTime.zero)
        self.player.pause()
        self.playerLayer.removeFromSuperlayer()
    }
    //MARK:- Function for restart Video
    func seekBackward() {
        let playerCurrentTime = CMTimeGetSeconds(player.currentTime())
        var newTime = playerCurrentTime - seekDuration
        if newTime < 0 {
            newTime = 0
        }
        let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        player.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
    }
    
    //MARK:- Function for restart Video
    func seekForward() {
        guard let duration  = player.currentItem?.duration else {
            return
        }
        let playerCurrentTime = CMTimeGetSeconds(player.currentTime())
        let newTime = playerCurrentTime + seekDuration
        if newTime < (CMTimeGetSeconds(duration) - seekDuration) {
            let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
            player.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            
        }
    }
    func changeVideoGravity(videoGravity:AVLayerVideoGravity){
        self.playerLayer.videoGravity = videoGravity
    }
}

extension SKPlayer {
    private func observeBuffering(for playerItem: AVPlayerItem) {
        self.isEmptyBuffer = playerItem.observe(\.isPlaybackBufferEmpty, changeHandler: self.emptyBuffer)
        self.isFullBuffer = playerItem.observe(\.isPlaybackBufferFull, changeHandler: self.fullBuffer)
        self.isLikelyBuffer = playerItem.observe(\.isPlaybackLikelyToKeepUp, changeHandler: self.likelyBuffer)
        
    }
    
    private func emptyBuffer(playerItem: AVPlayerItem, change: NSKeyValueObservedChange<Bool>) {
        if playerItem.isPlaybackBufferEmpty {
            self.spinner.startAnimating()
            print(playerItem.isPlaybackBufferEmpty)
        }
    }
    
    private func fullBuffer(playerItem: AVPlayerItem, change: NSKeyValueObservedChange<Bool>) {
        if playerItem.isPlaybackBufferFull {
            self.spinner.stopAnimating()
            print(playerItem.isPlaybackBufferFull)
        }
    }
    
    private func likelyBuffer(playerItem: AVPlayerItem, change: NSKeyValueObservedChange<Bool>) {
        self.spinner.stopAnimating()
        if playerItem.isPlaybackLikelyToKeepUp { print(playerItem.isPlaybackLikelyToKeepUp)}
    }
    func showLoader(view: UIView){
        spinner.backgroundColor = .red
        spinner.layer.cornerRadius = 3.0
        spinner.clipsToBounds = true
        spinner.hidesWhenStopped = true
        spinner.style = .medium
        spinner.center = view.center
        view.addSubview(spinner)
    }
    
}
// MARK:- extension for constant

extension SKPlayer {
    //MARK: FILTERS FOR VIDEO AND IMAGE
    enum SKFilter:String , CaseIterable {
        case CILineScreen = "CILineScreen"
        case CIDepthOfField = "CIDepthOfField"
        case CIEdgeWork = "CIEdgeWork"
        case CISpotColor = "CISpotColor"
        case CICircularScreen = "CICircularScreen"
        case CICMYKHalftone = "CICMYKHalftone"
        case CIDotScreen = "CIDotScreen"
        case CIHatchedScreen = "CIHatchedScreen"
        case CIBloom = "CIBloom"
        case CIGloom = "CIGloom"
        case CIEdges = "CIEdges"
        case CIBumpDistortion = "CIBumpDistortion"
        case CIGaussianBlur = "CIGaussianBlur"
        case CIPixellate = "CIPixellate"
        case CISepiaTone = "CISepiaTone"
        case CITwirlDistortion = "CITwirlDistortion"
        case CIUnsharpMask = "CIUnsharpMask"
        case CIVignette = "CIVignette"
    }
}

extension UIActivityIndicatorView {
    func dismissLoader() {self.stopAnimating() }
}
