//
//  ViewController.swift
//  VideoFilter
//
//  Created by Shubham Kaliyar on 13/12/20.
//  Copyright © 2020 2Build. All rights reserved.
//changeVideoGravity

import UIKit

class ViewController: UIViewController , FilterDidTabDelegate {
    
    
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var collectionView:FilterCollection!
    
    var skPlayer = SKPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.filterDelegate = self
        let videoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
        skPlayer.playVideo(videoURL: videoUrl, view: playerView, repeatedVideo: true)
        skPlayer.changeVideoGravity(videoGravity: .resizeAspect)
    }
    
    func didUpdateFilter(_ filter: SKPlayer.SKFilter) {
        skPlayer.appleFilter(filterName: filter)
    }
    
}




